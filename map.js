//初期マップとマーカー    
var marker = [];
var parkMarker = [];
var pediatricsMarker = [];
var hoikuData = [];
var parkData = [];
var pediatricsData = [];
var hoikuInfoWindow = [];
var parkInfoWindow = [];
var pediatricsInfoWindow = [];
function initMap() {
    var bunkyoCenter = {lat:35.718759, lng:139.745966};
    var map = new google.maps.Map(
        document.getElementById("map"), {zoom:14, center: bunkyoCenter, mapTypeId: google.maps.MapTypeId.ROADMAP});
    url = "hoiku.json";
    $.getJSON(url, (data) => {
        for (let index = 0; index < data.length; index ++) {
            hoikuData.push(data[index]);
            var posit = new google.maps.LatLng(data[index].lat, data[index].lng);
            if (hoikuData[index].type == 0){
                var type = "公立認可保育園"
            }else if (hoikuData[index].type == 1){
                var type = "公立認定こども園"
            }else if (hoikuData[index].type == 2){
                var type = "私立認可保育園"
            }else if (hoikuData[index].type == 3){
                var type = "私立小規模保育園"
            }else if (hoikuData[index].type == 4){
                var type = "私立家庭的保育事業"
            }else if (hoikuData[index].type == 5){
                var type = "私立事業所内保育事業"
            }
            if (hoikuData[index].train == 0){
                var train = "駅から徒歩5分以内"
            }else if (hoikuData[index].train == 1){
                var train = "駅から徒歩6~10分"
            }else if (hoikuData[index].train == 2){
                var train = "駅から徒歩10~15分"
            }
            marker[index] = new google.maps.Marker({
                position: posit,
                map: map,
            });
            
            hoikuInfoWindow[index] = new google.maps.InfoWindow({
                content: '<h5>' + hoikuData[index].name + '</h5>' + '<p>' + '<br>' + hoikuData[index].address + '<br>' + type + '<br>' + train + '</p>'
            });
            marker[index].addListener('click', function() {
                for (let index = 0; index < data.length; index ++) {
                    hoikuInfoWindow[index].close(map, marker[index]);
                }
                hoikuInfoWindow[index].open(map, marker[index]);
            });
        }
    });

    parkUrl = "park.json";
    $.getJSON(parkUrl, (data) => {
        for (let index = 0; index < data.length; index ++) {
            parkData.push(data[index]);
            var posit = new google.maps.LatLng(data[index].lat, data[index].lng);
            var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
            parkMarker[index] = new google.maps.Marker({
                position: posit,
                map: map,
                icon: {
                    fillColor: "#006400",                //塗り潰し色
                    fillOpacity: 0.2,                    //塗り潰し透過率
                    path: google.maps.SymbolPath.CIRCLE, //円を指定
                    scale: 16,                           //円のサイズ
                    strokeColor: "#006400",              //枠の色
                    strokeWeight: 1.0                    //枠の透過率
                }
            });
            
            parkInfoWindow[index] = new google.maps.InfoWindow({
                content: '<p>' + parkData[index].name + '<br>' + parkData[index].address + '</p>'
            });
            parkMarker[index].addListener('click', function() {
                for (let index = 0; index < data.length; index ++) {
                    parkInfoWindow[index].close(map, parkMarker[index]);
                }
                parkInfoWindow[index].open(map, parkMarker[index]);
            });
        }
    });

    pediatricsUrl = "pediatrics.json";
    $.getJSON(pediatricsUrl, (data) => {
        for (let index = 0; index < data.length; index ++) {
            pediatricsData.push(data[index]);
            var posit = new google.maps.LatLng(data[index].lat, data[index].lng);
            var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
            pediatricsMarker[index] = new google.maps.Marker({
                position: posit,
                map: map,
                icon: {
                    fillColor: "#ffa500",                //塗り潰し色
                    fillOpacity: 0.2,                    //塗り潰し透過率
                    path: google.maps.SymbolPath.CIRCLE, //円を指定
                    scale: 16,                           //円のサイズ
                    strokeColor: "#ffa500",              //枠の色
                    strokeWeight: 1.0                    //枠の透過率
                }
            });
            
            pediatricsInfoWindow[index] = new google.maps.InfoWindow({
                content: '<p>' + pediatricsData[index].施設名称 + '<br>' + pediatricsData[index].施設所在地 + '</p>'
            });
            pediatricsMarker[index].addListener('click', function() {
                for (let index = 0; index < data.length; index ++) {
                    pediatricsInfoWindow[index].close(map, pediatricsMarker[index]);
                }
                pediatricsInfoWindow[index].open(map, pediatricsMarker[index]);
            });
        }
    });
};
//選択したマーカーを表示する関数
function resultCheck(){
    for (let index = 0; index < hoikuData.length; index ++){
        marker[index].setVisible(false);
        hoikuInfoWindow[index].close(map, marker[index]);
    }
    for (let index = 0; index < parkData.length; index ++){
        parkMarker[index].setVisible(false);
        parkInfoWindow[index].close(map, parkMarker[index]);
    }
    for (let index = 0; index < pediatricsData.length; index ++){
        pediatricsMarker[index].setVisible(false);
        pediatricsInfoWindow[index].close(map, pediatricsMarker[index]);
    }
    for (let index = 0; index < hoikuData.length; index ++) {
        hoikuInfoWindow[index].close(map, marker[index]);
    }
    const type0 = document.getElementById("type0");
    const type1 = document.getElementById("type1");
    const type2 = document.getElementById("type2");
    const type3 = document.getElementById("type3");
    const type4 = document.getElementById("type4");
    const type5 = document.getElementById("type5");
    const time = document.getElementById("time").value;
    const park = document.getElementById("park");
    const pediatrics = document.getElementById("pediatrics");
    if (type0.checked) {
        for (let index = 0; index < hoikuData.length; index ++){
            if (hoikuData[index].type == 0){
                marker[index].addListener('click', function() {
                    for (let index = 0; index < hoikuData.length; index ++) {
                        hoikuInfoWindow[index].close(map, marker[index]);
                    }
                    hoikuInfoWindow[index].open(map, marker[index]);
                });
                marker[index].setVisible(true);
            }
        }
    }
    if (type1.checked) {
        for (let index = 0; index < hoikuData.length; index ++){
            if (hoikuData[index].type == 1){
                marker[index].addListener('click', function() {
                    for (let index = 0; index < hoikuData.length; index ++) {
                        hoikuInfoWindow[index].close(map, marker[index]);
                    }
                    hoikuInfoWindow[index].open(map, marker[index]);
                });
                marker[index].setVisible(true);
            }
        }
    }
    if (type2.checked) {
        for (let index = 0; index < hoikuData.length; index ++){
            if (hoikuData[index].type == 2){
                marker[index].addListener('click', function() {
                    for (let index = 0; index < hoikuData.length; index ++) {
                        hoikuInfoWindow[index].close(map, marker[index]);
                    }
                    hoikuInfoWindow[index].open(map, marker[index]);
                });
                marker[index].setVisible(true);
            }
        }
    }
    if (type3.checked) {
        for (let index = 0; index < hoikuData.length; index ++){
            if (hoikuData[index].type == 3){
                marker[index].addListener('click', function() {
                    for (let index = 0; index < hoikuData.length; index ++) {
                        hoikuInfoWindow[index].close(map, marker[index]);
                    }
                    hoikuInfoWindow[index].open(map, marker[index]);
                });
                marker[index].setVisible(true);
            }
        }
    }
    if (type4.checked) {
        for (let index = 0; index < hoikuData.length; index ++){
            if (hoikuData[index].type == 4){
                marker[index].addListener('click', function() {
                    for (let index = 0; index < hoikuData.length; index ++) {
                        hoikuInfoWindow[index].close(map, marker[index]);
                    }
                    hoikuInfoWindow[index].open(map, marker[index]);
                });
                marker[index].setVisible(true);
            }
        }
    }
    if (type5.checked) {
        for (let index = 0; index < hoikuData.length; index ++){
            if (hoikuData[index].type == 5){
                marker[index].addListener('click', function() {
                    for (let index = 0; index < hoikuData.length; index ++) {
                        hoikuInfoWindow[index].close(map, marker[index]);
                    }
                    hoikuInfoWindow[index].open(map, marker[index]);
                });
                marker[index].setVisible(true);
            }
        }
    }
    if (park.checked) {
        for (let index = 0; index < parkData.length; index ++){
            parkMarker[index].setVisible(true);
        }
    }
    if (pediatrics.checked) {
        for (let index = 0; index < pediatricsData.length; index ++){
            pediatricsMarker[index].setVisible(true);
        }
    }
    console.log(time);
    //time表示
    if (time == "time0"){
        console.log("0");
        for (let index = 0; index < hoikuData.length; index ++){
            if (hoikuData[index].train > 0){
                marker[index].setVisible(false);
            }
        }        
    }else if(time == "time1"){
        for (let index = 0; index < hoikuData.length; index ++){
            if (hoikuData[index].train > 1){
                marker[index].setVisible(false);
            }
        }
    }else if(time == "time2"){
        for (let index = 0; index < hoikuData.length; index ++){
            if (hoikuData[index].train > 2){
                marker[index].setVisible(false);
            }
        }
    }else if(time == "time3"){
        ;
    }
}
//種類ごとにマーカー色分け